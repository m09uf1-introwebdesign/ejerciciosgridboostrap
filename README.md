# CFGS - M09 Disseny d'Interfícies Web
## Introducció Responsive Design amb Bootstrap

En aquests exercicis usem Boostrap. 
Principals carecterístiques:
* Mobile First. Prioritza que el disseny funcion per a les pantalles més petites, les de móbils.
* Utilitzem el disseny de grid, que ens permet organitzar la info amb fins a 12 quadres per files.
* Podem tenir files infinites.

Recursos:
* [Boostrap en 5 minuts, video+codi](https://www.youtube.com/watch?v=yalxT0PEx8c)


#### EXERCICI 1 
Volem crear la secció de notícies d'un portal responsive amb 3 dissenys:

* Mobile, pantalla < 600px -> XS

  Es veuran 2 notícies per línia. No hi haurà menú lateral.

  	| 2 notícies |

* Tablet > 600, < 992 (tablet) -> SM

 Es veuran 4 notícies per línia. Hi haurà menú lateral.

  	| Menu | 4 notícies |

* Desktop > 992 -> MD

Es veuran 6 notícies per línia. Hi haurà menú lateral.

  	| Menu | 6 notícies | Anuncis | 

#### EXERCICI 2

Cal plantejar 4 dissenys d'un altre portal de notícies.

* Portrait i dimensions < 600 (mobile) -> XS

    | 1 notícia  |

*  Pantalla petita (tablet) > 600px, < 992px -> SM

    | 2 notícies |

*  Pantalla PC petit > 992px, < 1200 (laptop) -> MD

  	| Menu | 4 notícies |

*  Pantalla PC gran > 1200px (deskop) -> LG

    | Menu | 6 notícies | Anuncis |

### EXERCICI 3

Cal plantejar 2 dissenys de la secció d'ofertes del nostre canal de micromecenatge.

* Dimensions molt petites i petites (mobile, tablet) < 600 i > 600, < 992 -> SM i XS

	| Oferta 1 |

  	| Oferta 2 |

  	| Oferta 3 |

* Pantalla PC i pantalles més grans > 992px -> MD

	| Oferta 1 | Oferta 2 | Oferta 3 |
  
